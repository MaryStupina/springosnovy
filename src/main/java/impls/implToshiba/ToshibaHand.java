package impls.implToshiba;

import interfaces.Hand;

public class ToshibaHand implements Hand {
    public void catchSomething() {
        System.out.println("hand from Toshiba");
    }
}

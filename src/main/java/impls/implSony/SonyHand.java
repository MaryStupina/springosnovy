package impls.implSony;

import interfaces.Hand;

public class SonyHand implements Hand {
    public void catchSomething() {
        System.out.println("hand from Sony");
    }
}

import impls.implSony.SonyHead;
import impls.implSony.SonyLeg;
import impls.implToshiba.ToshibaHand;

public class RobotConstructor {

    public static void main(String[] args) {
        SonyHead sonyHead = new SonyHead();
        ToshibaHand toshibaHand = new ToshibaHand();
        SonyLeg sonyLeg = new SonyLeg();

        Robot robot = new Robot(toshibaHand, sonyHead, sonyLeg);

        robot.action();
    }
}

package robot;

import interfaces.Hand;
import interfaces.Head;
import interfaces.Leg;
import interfaces.Robot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

public abstract class BaseModel implements Robot {

    @Autowired
    @Qualifier("sonyHead")
    private Hand hand;

    @Autowired
    @Qualifier("sonyHand")
    private Head head;

    @Autowired
    @Qualifier("sonyLeg")
    private Leg leg;

    public BaseModel() {
        System.out.println(this + " - is BaseModel Constructor()");
    }

    public Hand getHand() {
        return hand;
    }

    public void setHand(Hand hand) {
        this.hand = hand;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public Leg getLeg() {
        return leg;
    }

    public void setLeg(Leg leg) {
        this.leg = leg;
    }
}

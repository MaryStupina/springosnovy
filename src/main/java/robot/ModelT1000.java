package robot;

import interfaces.Hand;
import interfaces.Head;
import interfaces.Leg;
import interfaces.Robot;

public class ModelT1000 extends BaseModel{

    private String color;
    private int year;
    private boolean soundEnabled;


    public ModelT1000(String color, int year, boolean soundEnabled) {
        this.color = color;
        this.year = year;
        this.soundEnabled = soundEnabled;
    }

    public ModelT1000(Hand hand, Head head, Leg leg, String color, int year, boolean soundEnabled) {
        this.color = color;
        this.year = year;
        this.soundEnabled = soundEnabled;
    }

    public ModelT1000() {

    }

    @Override
    public void action() {
        getHand().catchSomething();
        getHead().calc();
        getLeg().go();
        System.out.println(color);
        System.out.println(year);
        System.out.println(soundEnabled);

    }

    @Override
    public void dance() {
        System.out.println("ModelT1000 is dancing!");
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean isSoundEnabled() {
        return soundEnabled;
    }

    public void setSoundEnabled(boolean soundEnabled) {
        this.soundEnabled = soundEnabled;
    }

    public void initObject(){
        System.out.println("init");
    }

    public void destroyObject(){
        System.out.println("destroy");
    }
}
